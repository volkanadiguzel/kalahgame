/**
 * 
 */
package com.backbase.kalah.service.exception;

/**
 * This exception thrown when the move in Kalah game is illegal.
 * 
 * @author <a href="mailto:volkan.ad@gmail.com">Volkan Adiguzel</a>
 */
public class MoveIsNotLegalExeption extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8040929918253044702L;
	
	
	/**
	 * 
	 */
	public MoveIsNotLegalExeption(String message) {
		super(message);
	}

}
