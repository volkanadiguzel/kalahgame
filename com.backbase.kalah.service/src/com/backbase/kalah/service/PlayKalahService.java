/**
 * 
 */
package com.backbase.kalah.service;

import com.backbase.kalah.model.Board;
import com.backbase.kalah.service.exception.MoveIsNotLegalExeption;

/**
 * This interface contains service method signatures for kalah game. 
 * 
 * @author <a href="mailto:volkan.ad@gmail.com">Volkan Adiguzel</a>
 */
public interface PlayKalahService {
	
	/**
	 * This method is used to initialize a Kalah game. 
	 * It provides that by creating a board and setting initial pit count and initial stone count in a pit.
	 * 
	 * @author <a href="mailto:volkan.ad@gmail.com">Volkan Adiguzel</a>
	 */
	public Board initializeGame(int pitCount, int stoneCount);
	
	/**
	 * This method is used to make a move in a Kalah game. 
	 * Which player is to move is stored in Board object. movePit(the array index) is  used to determine which pit is to move. 
	 * The method saves the state of board object (Which player to move, is the game completed etc.).
	 * 
	 * 
	 * @author <a href="mailto:volkan.ad@gmail.com">Volkan Adiguzel</a>
	 */
	public void move(Board board, int movePit) throws MoveIsNotLegalExeption;

}
