/**
 * 
 */
package com.backbase.kalah.service;

import com.backbase.kalah.model.Board;
import com.backbase.kalah.model.PlayerSide;
import com.backbase.kalah.service.exception.MoveIsNotLegalExeption;

/**
 * This class represents the implementations of PlayKalahService interface methods.
 * 
 * @author <a href="mailto:volkan.ad@gmail.com">Volkan Adiguzel</a>
 */
public class PlayKalahServiceImpl implements PlayKalahService {

	/* (non-Javadoc)
	 * @see com.backbase.kalah.service.PlayKalahService#initializeGame(int, int)
	 */
	@Override
	public Board initializeGame(int pitCount, int stoneCount) {
		return new Board(pitCount, stoneCount);
	}
	
	
	/* (non-Javadoc)
	 * @see com.backbase.kalah.service.PlayKalahService#move(com.backbase.kalah.model.Board, int)
	 */
	@Override
	public void move(Board board, int movePitIndex) throws MoveIsNotLegalExeption {
		
		PlayerSide currentPlayerSide = null;
		PlayerSide opponentPlayerSide = null;

		if (board.isFirstPlayerToMove()) {
			currentPlayerSide = board.getFirstPlayerSide();
			opponentPlayerSide = board.getSecondPlayerSide();
		} else {
			currentPlayerSide = board.getSecondPlayerSide();
			opponentPlayerSide = board.getFirstPlayerSide();

		}
		
		checkMoveLegal(movePitIndex, currentPlayerSide);
		
		sawStones(board, movePitIndex, currentPlayerSide, opponentPlayerSide);
		
		checkGameComplete(board);
		
		return;
	}


	/**
	 * This method is used to sawing all stones from a defined pin.
	 * 
	 * @param board
	 * @param movePitIndex
	 * @param currentPlayerSide
	 * @param opponentPlayerSide
	 */
	private void sawStones(Board board, int movePitIndex, PlayerSide currentPlayerSide,
			PlayerSide opponentPlayerSide) {
		
		// Grab the stones.
		int stoneCountInPin = currentPlayerSide.getMyPits()[movePitIndex].getStoneCount();
		currentPlayerSide.getMyPits()[movePitIndex].setStoneCount(0);
		
		//Sow the stones until no left.
		int deliverStartPinIndex = movePitIndex + 1; //Starting index must be the next index.
		while (stoneCountInPin > 0) { 
			
			//Sow the stones to your side (starting from the next index).
			for (int i = deliverStartPinIndex; i < currentPlayerSide.getMyPits().length && stoneCountInPin > 0; i++) {
				currentPlayerSide.getMyPits()[i].setStoneCount(currentPlayerSide.getMyPits()[i].getStoneCount() + 1);
				stoneCountInPin--;
				
				if (stoneCountInPin == 0) {//last stone sowed.
					if (currentPlayerSide.getMyPits()[i].getStoneCount() == 1) { // last stone is sowed in an empty pit. 
						int opponentPitStoneCount = opponentPlayerSide.getMyPits()[opponentPlayerSide.getMyPits().length - i - 1].getStoneCount();
						currentPlayerSide.getMyHousePit().setStoneCount(currentPlayerSide.getMyHousePit().getStoneCount() + 1 + opponentPitStoneCount);
						currentPlayerSide.getMyPits()[i].setStoneCount(0);
						opponentPlayerSide.getMyPits()[opponentPlayerSide.getMyPits().length - i - 1].setStoneCount(0);
					}
										
					board.setFirstPlayerToMove(!board.isFirstPlayerToMove()); //change the turn
					break;
				}
			}
			
			deliverStartPinIndex = 0; // If so many stones to continue to the second tour, start sowing from the beginning.
			
			// Sow a stone in your house pit
			if (stoneCountInPin > 0) {
				currentPlayerSide.getMyHousePit().setStoneCount(currentPlayerSide.getMyHousePit().getStoneCount() + 1);
				stoneCountInPin--;
				
				if (stoneCountInPin == 0) { //last stone sowed in the house pit (don't change the turn)
					break;
				}
			}
			
			//Sow the stones in your opponent pits.
			for (int i = 0; i < opponentPlayerSide.getMyPits().length && stoneCountInPin > 0; i++) {
				opponentPlayerSide.getMyPits()[i].setStoneCount(opponentPlayerSide.getMyPits()[i].getStoneCount() + 1);
				stoneCountInPin--;
				
				if (stoneCountInPin == 0) {  //last stone sowed. 

					board.setFirstPlayerToMove(!board.isFirstPlayerToMove()); //change the turn
					break;
				}
			}
			
			//Skip the opponent house pit.
			
		}
	}

	/**
	 * This method is used if the game is complete. 
	 * To determine that, first check all pits on a side. 
	 * If all are empty, move all stones in other side to its house pit.
	 * If first side is not complete, check the other side.
	 * 
	 * @param board
	 * @return
	 */
	private void checkGameComplete(Board board) {
		
		boolean isGameComplete = true;
		//if any non empty pit found, game is not complete
		for (int i = 0; i < board.getFirstPlayerSide().getMyPits().length; i++) {
			if (board.getFirstPlayerSide().getMyPits()[i].getStoneCount() > 0) {
				isGameComplete = false;
			}
		}
		
		if (isGameComplete) { // all empty pits for one side. moving operation for the other side.
			int stonesToOpponent = 0;
			for (int i = 0; i < board.getSecondPlayerSide().getMyPits().length; i++) {
				stonesToOpponent = stonesToOpponent + board.getSecondPlayerSide().getMyPits()[i].getStoneCount();
				board.getSecondPlayerSide().getMyPits()[i].setStoneCount(0);
			}
			board.getSecondPlayerSide().getMyHousePit().setStoneCount(board.getSecondPlayerSide().getMyHousePit().getStoneCount() + stonesToOpponent);
			board.setGameComplete(true);
			
			return;
		}
		
		isGameComplete = true;
		//if any non empty pit found, game is not complete
		for (int i = 0; i < board.getSecondPlayerSide().getMyPits().length; i++) {
			if (board.getSecondPlayerSide().getMyPits()[i].getStoneCount() > 0) {
				isGameComplete = false;
			}
		}
		
		if (isGameComplete) { // all empty pits for one side. moving operation for the other side.
			int stonesToOpponent = 0;
			for (int i = 0; i < board.getFirstPlayerSide().getMyPits().length; i++) {
				stonesToOpponent = stonesToOpponent + board.getFirstPlayerSide().getMyPits()[i].getStoneCount();
				board.getFirstPlayerSide().getMyPits()[i].setStoneCount(0);
			}
			board.getFirstPlayerSide().getMyHousePit().setStoneCount(board.getFirstPlayerSide().getMyHousePit().getStoneCount() + stonesToOpponent);

			board.setGameComplete(true);
			
			return;
		}
		
		board.setGameComplete(false);

		
		
		return;
	}


	/**
	 * This method checks if move operation executed for an empty pit.
	 * 
	 * @param movePitNumber
	 * @param currentPlayerSide
	 * @throws MoveIsNotLegalExeption
	 */
	private void checkMoveLegal(int movePitNumber, PlayerSide currentPlayerSide) throws MoveIsNotLegalExeption {
		
		if (currentPlayerSide.getMyPits()[movePitNumber].getStoneCount() == 0) { 
			throw new MoveIsNotLegalExeption("Illegal move! There is no stone left in the pit.");
		}
		
		return;
	}

}
