/**
 * 
 */
package com.backbase.kalah.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.backbase.kalah.model.Board;
import com.backbase.kalah.model.Pit;
import com.backbase.kalah.service.PlayKalahService;
import com.backbase.kalah.service.PlayKalahServiceImpl;
import com.backbase.kalah.service.exception.MoveIsNotLegalExeption;

/**
 * This servlete class is used to provide playing the game. 
 * If the first player joins, redirecting him/her to waiting page (wait.jsp) until the second player joins the game.
 * After the second player joined, two player starts the move one by one. 
 * @author <a href="mailto:volkan.ad@gmail.com">Volkan Adiguzel</a>
 */
public class PlayKalahServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7843468996764330990L;

	private PlayKalahService playKalahService = new PlayKalahServiceImpl();

	public void init() throws ServletException {
	}

	@SuppressWarnings("unchecked")
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		ServletContext context = request.getSession().getServletContext();
		Map<String, Board> gameMap = (HashMap<String, Board>) context.getAttribute("gameMap");
		
		String waitingGameId = (String)context.getAttribute("waitingGameId");
		String gameId = (String) request.getSession().getAttribute("gameId");
		
		Board board = null;
		
		if (waitingGameId != null && gameId.equals(waitingGameId)) { // waiting for opponent
			response.sendRedirect("wait.jsp");
			return;
		} else {
			board = gameMap.get(gameId); // two player joined the game. 
		}
		
		if (request.getSession().getAttribute("player1Name") == null) { //setting the player name for view.
			request.getSession().setAttribute("player1Name", board.getFirstPlayerSide().getPlayerName());
		}		
		
		if (request.getSession().getAttribute("player2Name") == null) { //setting the player name for view.
			request.getSession().setAttribute("player2Name", board.getSecondPlayerSide().getPlayerName());
		}
		
		request.getSession().setAttribute("message", null); // initialize info message.



		if (request.getParameter("p1MoveIndex") != null) { // first player's move.
			if (board.isFirstPlayerToMove()) {
				try {
					playKalahService.move(board, new Integer(request.getParameter("p1MoveIndex")));
				} catch (NumberFormatException e) {
					request.getSession().setAttribute("message", "A Problem Occured.");
				} catch (MoveIsNotLegalExeption e) {
					request.getSession().setAttribute("message", "Illegal Move!!");
				}
			}
		}
		
		if (request.getParameter("p2MoveIndex") != null) { // second player's move.
			if (!board.isFirstPlayerToMove()) {
				try {
					playKalahService.move(board, new Integer(request.getParameter("p2MoveIndex")));
				} catch (NumberFormatException e) {
					request.getSession().setAttribute("message", "A Problem Occured.");
				} catch (MoveIsNotLegalExeption e) {
					request.getSession().setAttribute("message", "Illegal Move!!");
				}
			}
		}
		
		// setting the values for view (jsp)
		///////////
		int p1HouseStoneCount = board.getFirstPlayerSide().getMyHousePit().getStoneCount();
		request.getSession().setAttribute("p1HouseStoneCount", p1HouseStoneCount);
		request.getSession().setAttribute("p1Pits", board.getFirstPlayerSide().getMyPits());
		
		int p2HouseStoneCount = board.getSecondPlayerSide().getMyHousePit().getStoneCount();
		request.getSession().setAttribute("p2HouseStoneCount", p2HouseStoneCount);
		
		Pit[] p2Pits = new Pit[board.getSecondPlayerSide().getMyPits().length];
		
		for (int i = 0; i < p2Pits.length; i++) {
			p2Pits[p2Pits.length - i - 1] = board.getSecondPlayerSide().getMyPits()[i];
		}
		
		request.getSession().setAttribute("p2Pits", p2Pits);
		
		if (board.isGameComplete()) {
			String gameOverMessage = null;
			if (p1HouseStoneCount > p2HouseStoneCount) {
				gameOverMessage = "Game Over!!" + request.getSession().getAttribute("player1Name") +  " wins.";
			} else if (p1HouseStoneCount < p2HouseStoneCount) {
				gameOverMessage = "Game Over!!" + request.getSession().getAttribute("player2Name") + " wins.";
			} else {
				gameOverMessage = "Game Over!! Tie game.";
			}
			request.getSession().setAttribute("message", gameOverMessage);

			request.getSession().setAttribute("turn", "over");

		} else {
		
			if (board.isFirstPlayerToMove()) {
				request.getSession().setAttribute("turn", "p1");
			} else {
				request.getSession().setAttribute("turn", "p2");	
			}
		}
		///////////
		gameMap.put(gameId, board);
		context.setAttribute("gameMap", gameMap);
		
		response.sendRedirect("kalah.jsp");

	}

	public void destroy() {
		// do nothing.
	}
}
