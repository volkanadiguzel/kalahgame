/**
 * 
 */
package com.backbase.kalah.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.backbase.kalah.model.Board;
import com.backbase.kalah.service.PlayKalahService;
import com.backbase.kalah.service.PlayKalahServiceImpl;

/** 
 * This Servlet class captures the name of a player and starts the game. It is called after enter.jsp form submitted(the jsp for join the game).
 * In servlet context, all games keeping in an map named gameMap and after two player joined the game, game starts.
 * When first player joined the game, a game creating and keeping in servlet context as waitingGame. 
 * After second player joined the game, this waiting game is moved to gameMap and waitingGame removed from the context.
 * 
 * @author <a href="mailto:volkan.ad@gmail.com">Volkan Adiguzel</a>
 */
public class EnterKalahServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7290546560277426331L;
	
	private PlayKalahService playKalahService = new PlayKalahServiceImpl();
	
	/* (non-Javadoc)
	 * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		ServletContext context = config.getServletContext();
		if (context.getAttribute("gameMap") == null) { // First time creating the
			Map<String, Board> gameMap = new HashMap<String, Board>();
			context.setAttribute("gameMap", gameMap);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletContext context = request.getSession().getServletContext();
		
		String playerName = request.getParameter("name"); // Getting the players name.
		Map<String, Board> gameMap = (HashMap<String, Board>) context.getAttribute("gameMap"); // Getting the all games from the context.
		
		
		if (context.getAttribute("waitingGame") == null) { // First player joined. Creating a waiting game in the context. 
			Board board = playKalahService.initializeGame(6, 6);
			board.getFirstPlayerSide().setPlayerName(playerName);
			context.setAttribute("waitingGame", board);
			
			String gameId = UUID.randomUUID().toString();
			context.setAttribute("waitingGameId", gameId);
			
			request.getSession().setAttribute("gameId", gameId);
			request.getSession().setAttribute("playerOrder", "player1");

		} else {											//Second player joined to the game waiting already.
			Board board = (Board) context.getAttribute("waitingGame");
			
			board.getSecondPlayerSide().setPlayerName(playerName);
			
			String gameId = (String) context.getAttribute("waitingGameId");
			gameMap.put(gameId, board);
			context.setAttribute("gameMap", gameMap);
			context.setAttribute("waitingGame", null);
			context.setAttribute("waitingGameId", null);
			
			request.getSession().setAttribute("gameId", gameId);
			request.getSession().setAttribute("playerOrder", "player2");
			
		}
		
		//A new game is started. Empty user names. 
		request.getSession().setAttribute("player1Name", null);
		request.getSession().setAttribute("player2Name", null);
		
		response.sendRedirect(request.getContextPath() + "/PlayKalah");

		

	}
	
	public void destroy() {
		// do nothing.
	}

}
