<!-- 
	This jsp is used to play the game. It contains all game board view and hides the irrelevant parts according to player and players turn.
 -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="refresh" content="5; url=${pageContext.request.contextPath}/PlayKalah">

<title>Play Kalah!!</title>
</head>
<body>

	<h3><c:out value="${message}" /></h3>
	<table>
		<tr>
		
			<td>
				<c:out value="${player2Name}" />&nbsp;&nbsp;&nbsp;&nbsp; <c:if test="${(turn == 'p2')}"> --> </c:if>
			</td>
			<td>
				<c:out value="${p2HouseStoneCount}" /> &nbsp; || &nbsp; 
			</td>
			<td>
				<c:forEach items="${p2Pits}" var="pit" varStatus="loop">
					<c:if test="${(turn == 'p2' && playerOrder == 'player2')}">
				
					<a href="${pageContext.request.contextPath}/PlayKalah?p2MoveIndex=<c:out value="${fn:length(p2Pits) - 1 - loop.index}" />">
						<c:out value="${pit.stoneCount}" />
					</a>
					</c:if>
					<c:if test="${(turn != 'p2' || playerOrder != 'player2')}">
						<c:out value="${pit.stoneCount}" />
					</c:if>					
					
					
					 &nbsp;
				</c:forEach>
				||
			</td>
			<td>
			</td>
		</tr>
		<tr>
			<td>
				<c:out value="${player1Name}" />&nbsp;&nbsp;&nbsp;&nbsp; <c:if test="${(turn == 'p1')}"> --> </c:if>
			</td>
			<td>
			</td>
			<td>
				<c:forEach items="${p1Pits}" var="pit" varStatus="loop">
					<c:if test="${(turn == 'p1' && playerOrder == 'player1')}">
					<a href="${pageContext.request.contextPath}/PlayKalah?p1MoveIndex=<c:out value="${loop.index}" />">
						<c:out value="${pit.stoneCount}" />
					</a>
					</c:if>
					<c:if test="${(turn != 'p1' || playerOrder != 'player1')}">
						<c:out value="${pit.stoneCount}" />
					</c:if>					
					
					 &nbsp;
				</c:forEach>
				||
			</td>
			<td>
				 &nbsp; &nbsp;<c:out value="${p1HouseStoneCount}" /> &nbsp;				
			</td>
		</tr>

	</table>

</body>
</html>