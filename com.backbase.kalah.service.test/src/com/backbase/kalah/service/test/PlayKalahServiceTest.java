/**
 * 
 */
package com.backbase.kalah.service.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.backbase.kalah.model.Board;
import com.backbase.kalah.service.PlayKalahService;
import com.backbase.kalah.service.PlayKalahServiceImpl;

/**
 * This test class is written for PlayKalahService / PlayKalahServiceImpl services.
 * 
 * @author <a href="mailto:volkan.ad@gmail.com">Volkan Adiguzel</a>
 */
public class PlayKalahServiceTest {
	
	
	@Test
	public void testInitialize() throws Exception {
		
		PlayKalahService playKalahService = new PlayKalahServiceImpl();
		Board board_6_4 = playKalahService.initializeGame(6, 4);
		
		assertEquals(6, board_6_4.getFirstPlayerSide().getMyPits().length);
		assertEquals(6, board_6_4.getSecondPlayerSide().getMyPits().length);
		
		assertEquals(4, board_6_4.getFirstPlayerSide().getMyPits()[0].getStoneCount());
		assertEquals(4, board_6_4.getSecondPlayerSide().getMyPits()[0].getStoneCount());
		
		assertEquals(0, board_6_4.getFirstPlayerSide().getMyHousePit().getStoneCount());
		assertEquals(0, board_6_4.getSecondPlayerSide().getMyHousePit().getStoneCount());
		
		assertEquals(true, board_6_4.isFirstPlayerToMove());
		
		Board board_8_5 = playKalahService.initializeGame(8, 5);
		
		assertEquals(8, board_8_5.getFirstPlayerSide().getMyPits().length);
		assertEquals(8, board_8_5.getSecondPlayerSide().getMyPits().length);
		
		assertEquals(5, board_8_5.getFirstPlayerSide().getMyPits()[0].getStoneCount());
		assertEquals(5, board_8_5.getSecondPlayerSide().getMyPits()[0].getStoneCount());
		
		assertEquals(0, board_8_5.getFirstPlayerSide().getMyHousePit().getStoneCount());
		assertEquals(0, board_8_5.getSecondPlayerSide().getMyHousePit().getStoneCount());
		
		assertEquals(true, board_8_5.isFirstPlayerToMove());
		
	}
	
	@Test
	public void testMove() throws Exception {
		PlayKalahService playKalahService = new PlayKalahServiceImpl();
		Board board = playKalahService.initializeGame(6, 6);
		
		playKalahService.move(board, 2);
		assertEquals(6, board.getFirstPlayerSide().getMyPits()[0].getStoneCount());
		assertEquals(6, board.getFirstPlayerSide().getMyPits()[1].getStoneCount());
		assertEquals(0, board.getFirstPlayerSide().getMyPits()[2].getStoneCount());
		assertEquals(7, board.getFirstPlayerSide().getMyPits()[3].getStoneCount());
		assertEquals(7, board.getFirstPlayerSide().getMyPits()[4].getStoneCount());
		assertEquals(7, board.getFirstPlayerSide().getMyPits()[5].getStoneCount());
		assertEquals(1, board.getFirstPlayerSide().getMyHousePit().getStoneCount());
		assertEquals(7, board.getSecondPlayerSide().getMyPits()[0].getStoneCount());
		assertEquals(7, board.getSecondPlayerSide().getMyPits()[1].getStoneCount());
		assertEquals(6, board.getSecondPlayerSide().getMyPits()[2].getStoneCount());
		assertEquals(6, board.getSecondPlayerSide().getMyPits()[3].getStoneCount());
		assertEquals(6, board.getSecondPlayerSide().getMyPits()[4].getStoneCount());
		assertEquals(6, board.getSecondPlayerSide().getMyPits()[5].getStoneCount());
		assertEquals(0, board.getSecondPlayerSide().getMyHousePit().getStoneCount());
		assertEquals(false, board.isFirstPlayerToMove());

		
	}
	
	@Test
	public void testMove2() throws Exception {
		PlayKalahService playKalahService = new PlayKalahServiceImpl();
		Board board = playKalahService.initializeGame(6, 6);
		
		playKalahService.move(board, 0);
		assertEquals(0, board.getFirstPlayerSide().getMyPits()[0].getStoneCount());
		assertEquals(7, board.getFirstPlayerSide().getMyPits()[1].getStoneCount());
		assertEquals(7, board.getFirstPlayerSide().getMyPits()[2].getStoneCount());
		assertEquals(7, board.getFirstPlayerSide().getMyPits()[3].getStoneCount());
		assertEquals(7, board.getFirstPlayerSide().getMyPits()[4].getStoneCount());
		assertEquals(7, board.getFirstPlayerSide().getMyPits()[5].getStoneCount());
		assertEquals(1, board.getFirstPlayerSide().getMyHousePit().getStoneCount());
		assertEquals(6, board.getSecondPlayerSide().getMyPits()[0].getStoneCount());
		assertEquals(6, board.getSecondPlayerSide().getMyPits()[1].getStoneCount());
		assertEquals(6, board.getSecondPlayerSide().getMyPits()[2].getStoneCount());
		assertEquals(6, board.getSecondPlayerSide().getMyPits()[3].getStoneCount());
		assertEquals(6, board.getSecondPlayerSide().getMyPits()[4].getStoneCount());
		assertEquals(6, board.getSecondPlayerSide().getMyPits()[5].getStoneCount());
		assertEquals(0, board.getSecondPlayerSide().getMyHousePit().getStoneCount());
		assertEquals(true, board.isFirstPlayerToMove());

		///////////////////////
		
		playKalahService.move(board, 5);
		assertEquals(0, board.getFirstPlayerSide().getMyPits()[0].getStoneCount());
		assertEquals(7, board.getFirstPlayerSide().getMyPits()[1].getStoneCount());
		assertEquals(7, board.getFirstPlayerSide().getMyPits()[2].getStoneCount());
		assertEquals(7, board.getFirstPlayerSide().getMyPits()[3].getStoneCount());
		assertEquals(7, board.getFirstPlayerSide().getMyPits()[4].getStoneCount());
		assertEquals(0, board.getFirstPlayerSide().getMyPits()[5].getStoneCount());
		assertEquals(2, board.getFirstPlayerSide().getMyHousePit().getStoneCount());
		assertEquals(7, board.getSecondPlayerSide().getMyPits()[0].getStoneCount());
		assertEquals(7, board.getSecondPlayerSide().getMyPits()[1].getStoneCount());
		assertEquals(7, board.getSecondPlayerSide().getMyPits()[2].getStoneCount());
		assertEquals(7, board.getSecondPlayerSide().getMyPits()[3].getStoneCount());
		assertEquals(7, board.getSecondPlayerSide().getMyPits()[4].getStoneCount());
		assertEquals(7, board.getSecondPlayerSide().getMyPits()[5].getStoneCount());
		assertEquals(0, board.getSecondPlayerSide().getMyHousePit().getStoneCount());
		assertEquals(false, board.isFirstPlayerToMove());
		
		
	}
	
	@Test
	public void testGameComplete() throws Exception {
		/*
		 *	p1: 	24	|| 3 2 5 1 0 4
		 *	p2			|| 0 0 0 1 0 0 ||	32
		 *
		 */
		
		PlayKalahService playKalahService = new PlayKalahServiceImpl();
		Board board = playKalahService.initializeGame(6, 6);
		
		board.getFirstPlayerSide().getMyPits()[0].setStoneCount(0);
		board.getFirstPlayerSide().getMyPits()[1].setStoneCount(0);
		board.getFirstPlayerSide().getMyPits()[2].setStoneCount(0);
		board.getFirstPlayerSide().getMyPits()[3].setStoneCount(1);
		board.getFirstPlayerSide().getMyPits()[4].setStoneCount(0);
		board.getFirstPlayerSide().getMyPits()[5].setStoneCount(0);
		board.getFirstPlayerSide().getMyHousePit().setStoneCount(32);
		
		board.getSecondPlayerSide().getMyPits()[0].setStoneCount(4);
		board.getSecondPlayerSide().getMyPits()[1].setStoneCount(0);
		board.getSecondPlayerSide().getMyPits()[2].setStoneCount(1);
		board.getSecondPlayerSide().getMyPits()[3].setStoneCount(5);
		board.getSecondPlayerSide().getMyPits()[4].setStoneCount(2);
		board.getSecondPlayerSide().getMyPits()[5].setStoneCount(3);
		board.getSecondPlayerSide().getMyHousePit().setStoneCount(24);
		
		board.setFirstPlayerToMove(true);
		board.setGameComplete(false);
		
		playKalahService.move(board, 3);
		
		assertEquals(0, board.getFirstPlayerSide().getMyPits()[0].getStoneCount());
		assertEquals(0, board.getFirstPlayerSide().getMyPits()[1].getStoneCount());
		assertEquals(0, board.getFirstPlayerSide().getMyPits()[2].getStoneCount());
		assertEquals(0, board.getFirstPlayerSide().getMyPits()[3].getStoneCount());
		assertEquals(0, board.getFirstPlayerSide().getMyPits()[4].getStoneCount());
		assertEquals(0, board.getFirstPlayerSide().getMyPits()[5].getStoneCount());
		assertEquals(33, board.getFirstPlayerSide().getMyHousePit().getStoneCount());
		assertEquals(0, board.getSecondPlayerSide().getMyPits()[0].getStoneCount());
		assertEquals(0, board.getSecondPlayerSide().getMyPits()[1].getStoneCount());
		assertEquals(0, board.getSecondPlayerSide().getMyPits()[2].getStoneCount());
		assertEquals(0, board.getSecondPlayerSide().getMyPits()[3].getStoneCount());
		assertEquals(0, board.getSecondPlayerSide().getMyPits()[4].getStoneCount());
		assertEquals(0, board.getSecondPlayerSide().getMyPits()[5].getStoneCount());
		assertEquals(39, board.getSecondPlayerSide().getMyHousePit().getStoneCount());
		assertEquals(true, board.isGameComplete());
		
		
	}

}
