/**
 * 
 */
package com.backbase.kalah.model;

import java.io.Serializable;

/**
 * This class is the Pit class in Kalah game. 
 * Pits are used to keep some stones in the game.
 * 
 * @author <a href="mailto:volkan.ad@gmail.com">Volkan Adiguzel</a> 
 *
 *
 */
public class Pit implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1177998171901192313L;
	
	private int stoneCount;
	
	/**
	 * 
	 */
	public Pit(int stoneCount) {
		this.stoneCount = stoneCount;
	}

	/**
	 * @return the stoneCount
	 */
	public int getStoneCount() {
		return stoneCount;
	}

	/**
	 * @param stoneCount the stoneCount to set
	 */
	public void setStoneCount(int stoneCount) {
		this.stoneCount = stoneCount;
	}
	
	

}
