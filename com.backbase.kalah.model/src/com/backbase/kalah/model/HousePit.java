/**
 * 
 */
package com.backbase.kalah.model;

/**
 * This class is used for a special Pit (House pit or Kalah) in Kalah game.
 * 
 * @author <a href="mailto:volkan.ad@gmail.com">Volkan Adiguzel</a>
 */
public class HousePit extends Pit{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6955331126557055501L;

	/**
	 * 
	 */
	public HousePit() {
		super(0);
	}

}
