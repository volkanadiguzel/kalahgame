/**
 * 
 */
package com.backbase.kalah.model;

import java.io.Serializable;

/**
 * These class contains the all pits and information for a player in Kalah game.
 * 
 * @author <a href="mailto:volkan.ad@gmail.com">Volkan Adiguzel</a>
 */
public class PlayerSide implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1639619657445544131L;

	private Pit[] myPits;
	
	private HousePit myHousePit;
	
	private String playerName;


	/**
	 * @param pitCount: Number of pits for a player (except house pit).
	 * @param stoneCount: Stone count in each pit initially.
	 */
	public PlayerSide(int pitCount, int stoneCount) {
		this.myPits = new Pit[pitCount];
		
		for (int i = 0; i < this.myPits.length; i++) {
			this.myPits[i] = new Pit(stoneCount);
		}
		
		this.myHousePit = new HousePit();
	}

	/**
	 * @return the myPits
	 */
	public Pit[] getMyPits() {
		return myPits;
	}

	/**
	 * @param myPits the myPits to set
	 */
	public void setMyPits(Pit[] myPits) {
		this.myPits = myPits;
	}

	/**
	 * @return the myHousePit
	 */
	public HousePit getMyHousePit() {
		return myHousePit;
	}

	/**
	 * @param myHousePit the myHousePit to set
	 */
	public void setMyHousePit(HousePit myHousePit) {
		this.myHousePit = myHousePit;
	}

	/**
	 * @return the playerName
	 */
	public String getPlayerName() {
		return playerName;
	}

	/**
	 * @param playerName the playerName to set
	 */
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	
}
