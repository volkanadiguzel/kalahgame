/**
 * 
 */
package com.backbase.kalah.model;

import java.io.Serializable;

/**
 * This class is used to represent Board class for the Kalah game. 
 * This class contains all state of a game.
 * 
 * @author <a href="mailto:volkan.ad@gmail.com">Volkan Adiguzel</a>
 * 
 */
public class Board implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5524022682548174148L;

	private PlayerSide firstPlayerSide;
	
	private PlayerSide secondPlayerSide;

	private boolean firstPlayerToMove; 		//determines which player to move.
	
	private boolean gameComplete; 			// determines if the game is complete.
	
	/**
	 * @param pitCount: Number of pits for a player (except house pit).
	 * @param stoneCount: Stone count in each pit initially.
	 * 
	 */
	public Board(int pitCount, int stoneCount) {
		this.firstPlayerSide = new PlayerSide(pitCount, stoneCount);
		this.secondPlayerSide = new PlayerSide(pitCount, stoneCount);
		
		this.firstPlayerToMove = true;
		this.gameComplete = false;
	}

	/**
	 * @return the firstPlayerSide
	 */
	public PlayerSide getFirstPlayerSide() {
		return firstPlayerSide;
	}

	/**
	 * @param firstPlayerSide the firstPlayerSide to set
	 */
	public void setFirstPlayerSide(PlayerSide firstPlayerSide) {
		this.firstPlayerSide = firstPlayerSide;
	}

	/**
	 * @return the secondPlayerSide
	 */
	public PlayerSide getSecondPlayerSide() {
		return secondPlayerSide;
	}

	/**
	 * @param secondPlayerSide the secondPlayerSide to set
	 */
	public void setSecondPlayerSide(PlayerSide secondPlayerSide) {
		this.secondPlayerSide = secondPlayerSide;
	}

	/**
	 * @return the firstPlayerToMove
	 */
	public boolean isFirstPlayerToMove() {
		return firstPlayerToMove;
	}

	/**
	 * @param firstPlayerToMove the firstPlayerToMove to set
	 */
	public void setFirstPlayerToMove(boolean firstPlayerToMove) {
		this.firstPlayerToMove = firstPlayerToMove;
	}

	/**
	 * @return the gameComplete
	 */
	public boolean isGameComplete() {
		return gameComplete;
	}

	/**
	 * @param gameComplete the gameComplete to set
	 */
	public void setGameComplete(boolean gameComplete) {
		this.gameComplete = gameComplete;
	}
	
}
